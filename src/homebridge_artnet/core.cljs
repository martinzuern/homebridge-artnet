(ns homebridge-artnet.core
  (:use-macros [cljs.core.async.macros :only [go go-loop]])
  (:require [homebridge-artnet.animation :as anim]
            [clojure.set]
            [cljs.core.async :refer [<! timeout]]
            [oops.core :refer [oget oset! ocall oapply ocall! oapply! oget+ oset!+ ocall+ oapply+ ocall!+ oapply!+]]
            [schema.core :as s :include-macros true]
            [schema.coerce :as coerce :include-macros true]
            ["artnet" :as artnet]))

(def loop-timeout
  (/ 1000 60))                                              ; 60 Hz

;; State
(def context (atom {}))
(def registered-accessories (atom {}))


;; Validations
(def LightSchema
  "A schema for a nested data type"
  {:name     s/Str
   :type     s/Str                                          ; TODO: Add validation of correct types
   :channels [s/Int]})
(def ConfigSchema
  "A schema for a nested data type"
  {:platform (s/eq "ArtNet")
   :name     s/Str
   :options  {:universe s/Int
              :host     s/Str}
   :lights   [LightSchema]})
(def parse-config
  (coerce/coercer! ConfigSchema coerce/json-coercion-matcher))

;; Helpers
(defn update-values [m f & args]
  ; from http://blog.jayfields.com/2011/08/clojure-apply-function-to-each-value-of.html
  (reduce (fn [r [k v]] (assoc r k (apply f v args))) {} m))

(defn hsv->rgb
  "HSV (Hue, Saturation, Value) to RGB conversion."
  ; from https://github.com/rm-hull/inkspot/blob/ccb7a452a0930f451bcc6c960024d6029f616cd2/src/inkspot/converter.cljc#L30
  [[h s v]]
  (if (zero? s)
    (let [v (* v 255)]
      [v v v])
    (let [h (mod (/ h 60) 6)
          i (int h)
          f (if (even? i) (- 1 (- h i))
                (- h i))
          v (* v 255.0)
          m (* v (- 1.0 s))
          n (* v (- 1.0 (* s f)))]
      (case i
        0 [v n m]
        1 [n v m]
        2 [m v n]
        3 [m n v]
        4 [n m v]
        [v m n]))))

(defn brightness-by-on
  "Updates Brightness key based on On state."
  [coll]
  (let [on-int (if (:On coll) 1 0)]
    (update coll :Brightness #(* on-int %))))

(defn generate-uuid
  "Generates the UUID for a lamp."
  [config]
  (let [homebridge (:homebridge @context)
        seed       (-> config :channels str)]
    (homebridge.hap.uuid.generate seed)))

(defn get-context
  [accessory]
  (-> accessory (oget :context) (js->clj :keywordize-keys true)))

(def get-service-class
  (memoize (fn [service-name] (-> (:homebridge @context) (oget+ :hap :Service service-name)))))

(def get-characteristic-class
  (memoize (fn [characteristic-name] (-> (:homebridge @context) (oget+ :hap :Characteristic characteristic-name)))))

(defn get-service
  "docstring"
  [^js accessory service-name]
  (let [service-class (get-service-class service-name)]
    (or (.getService accessory service-class)
        (.addService accessory service-class (:name (get-context accessory))))))

(defn get-characteristic
  "Gets the characteristic of accessory."
  [^js service characteristic-name]
  (.getCharacteristic service (get-characteristic-class characteristic-name)))

(defn add-characteristic-val
  "Adds characteristic's value to coll, if not present already."
  [coll service cname]
  (if (contains? coll cname)
    coll
    (assoc coll cname (oget (get-characteristic service cname) :value))))



;; Lamp types

(def characteristics-by-type
  {:PAR {:Lightbulb [:On :Brightness]}
   :RGB {:Lightbulb [:On :Brightness :Hue :Saturation]}})


(defmulti set-handler! (fn [accessory & _]
                         (-> (get-context accessory) :type keyword)))

(defmethod set-handler! :PAR [accessory service characteristic-name value]
  (let [[channel] (:channels (get-context accessory))
        characteristics (-> {characteristic-name value}
                            (add-characteristic-val service :On)
                            (add-characteristic-val service :Brightness)
                            (brightness-by-on))]
    (anim/add-animation! channel (-> (:Brightness characteristics) (* 2.55)))))

(defmethod set-handler! :RGB [accessory service characteristic-name value]
  (let [[channel-r channel-g channel-b] (:channels (get-context accessory))
        characteristics (-> {characteristic-name value}
                            (add-characteristic-val service :On)
                            (add-characteristic-val service :Brightness)
                            (add-characteristic-val service :Hue)
                            (add-characteristic-val service :Saturation)
                            (brightness-by-on)
                            (update :Brightness #(/ % 100))
                            (update :Saturation #(/ % 100)))
        rgb             (hsv->rgb (mapv #(get characteristics %)
                                        [:Hue :Saturation :Brightness]))]
    (anim/add-animation! {channel-r (nth rgb 0)
                          channel-g (nth rgb 1)
                          channel-b (nth rgb 2)})))

(defn set-characteristics
  "docstring"
  [accessory]
  (let [type     (-> (get-context accessory) :type keyword)
        services (-> characteristics-by-type (get type) (keys))]

    (doseq [service-name services]
      (let [service         (get-service accessory service-name)
            characteristics (get-in characteristics-by-type [type service-name])]

        (doseq [characteristic-name characteristics]
          ; Initialize values
          (set-handler! accessory service)

          ; Set Handler
          (.on ^js (get-characteristic service characteristic-name)
               "set"
               (fn [value callback]
                 (set-handler! accessory service characteristic-name value)
                 (callback))))))))


(defn accessory-factory
  "Generates a new accessory."
  [configuration]
  (let [homebridge (:homebridge @context)]
    (doto (homebridge.platformAccessory. (:name configuration) (generate-uuid configuration))
      (oset! :context (clj->js configuration)))))

(defn artnet-client-factory
  "Builds the artnet client"
  [conf]
  (let [^js artnet-client (-> (:options conf)
                              (select-keys [:host])
                              (assoc :sendAll true)
                              (assoc :refresh 500)
                              (clj->js)
                              (artnet))
        loop-running      (atom false)
        universe          (-> conf :options :universe)
        set-fn            #(.set artnet-client universe 1 (clj->js %))
        channels          (->> (:lights conf) (map :channels) (flatten))
        null-channels     (-> (apply max channels) (repeat 0) (vec))
        state             (atom null-channels)]

    (set-fn @state)                                         ; blackout

    {:start-loop (fn []
                   (when-not @loop-running
                     (reset! loop-running true)
                     (go-loop []
                       (when @loop-running
                         (<! (timeout loop-timeout))
                         (anim/process-animations! state)
                         (set-fn @state)
                         (recur)))))
     :stop-loop  (fn []
                   (reset! loop-running false)
                   (set-fn null-channels)
                   (.close artnet-client))}))

(defn platform-factory
  "Constructor to set up the platform"
  [log, config, ^js api]
  (swap! context assoc :log log :config config :api api)
  (let [homebridge  (:homebridge @context)
        parsed-conf (-> (js->clj config :keywordize-keys true) (parse-config))
        {:keys [start-loop stop-loop]} (artnet-client-factory parsed-conf)]

    ; Homebridge is done restoring caches accessories
    (.on api "didFinishLaunching"
         (fn []
           (log "DidFinishLaunching - Listing cached accessories:" (clj->js @registered-accessories))
           (let [config-acc       (->> (:lights parsed-conf)
                                       (map (fn [el] [(generate-uuid el) el]))
                                       (into {}))
                 uuids-registered (-> @registered-accessories (keys) (set))
                 uuids-config     (-> config-acc (keys) (set))
                 vals-or-empty    #(or (vals %) ())]
             
             ;; Remove no longer listed accessories
             (let [remove-uuids (clojure.set/difference uuids-registered uuids-config)
                   remove-accs  (select-keys @registered-accessories remove-uuids)]
               (.unregisterPlatformAccessories api "homebridge-artnet" "ArtNet" (clj->js (vals-or-empty remove-accs)))
               (swap! registered-accessories #(apply dissoc % remove-uuids)))

             ;; Add new accessories
             (let [add-uuids (clojure.set/difference uuids-config uuids-registered)
                   add-accs  (-> config-acc (select-keys add-uuids) (update-values accessory-factory))]
               (.registerPlatformAccessories api "homebridge-artnet" "ArtNet" (clj->js (vals-or-empty add-accs)))
               (swap! registered-accessories conj add-accs))

             ;; Reset characteristics
             (doseq [acc (vals @registered-accessories)]
               (set-characteristics acc))

             ;; Starting Loop
             (start-loop))))

    (.on api "shutdown"
         (fn [] (stop-loop)))

    (clj->js {"configureAccessory" (fn [acc]
                                     (swap! registered-accessories assoc (oget acc :UUID) acc))})))


;; Homebridge constructor
(defn homebridge-init!
  "This is where we initialize the accessory-platform, the only function that will be exported."
  [^js homebridge]
  (when-not (nil? homebridge)
    (swap! context assoc :homebridge homebridge)
    (.registerPlatform homebridge "homebridge-artnet" "ArtNet" platform-factory false)))


