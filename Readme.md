# Homebridge-Artnet

[![npm version](https://badge.fury.io/js/homebridge-artnet.svg)](https://badge.fury.io/js/homebridge-artnet)


## Introduction

Adds support for [Art-Net](https://en.wikipedia.org/wiki/Art-Net) lights to Homebridge.



## Usage

Currently plain lights with one channel ("PAR") and RGB lights with three channels ("RGB") are supported.

In Homebridge's config, use the following settings:

```json
{
  "bridge": { [...] },
  "accessories": [],
  "platforms": [{
    "platform": "ArtNet",
    "name": "Artnet Universe",
    "options": {
      "universe": 0,
      "host": "192.168.0.10"
    },
    "lights": [
      {"name": "Left", "type": "PAR", "channels": [1]},
      {"name": "Right", "type": "PAR", "channels": [2]},
      {"name": "PAR 5", "type": "RGB", "channels": [5, 6, 7]},
      {"name": "PAR 6", "type": "RGB", "channels": [10, 11, 12]}
    ]
  }]
}
```



## Development

**Setup**:

1. [Install Clojure](https://clojure.org/guides/getting_started)
2. run `npm install` and `lein classpath`
3. to build, run `npm run build`
4. ‘Install’ package locally using `npm link`
5. Install and run homebridge; add accessory and test :)



## Thanks to

- Artnet library: [hobbyquaker/artnet](https://github.com/hobbyquaker/artnet)
- Schema validation: prismatic/schema
- JavaScript Interop: binaryage/oops
- update-values fn:*http://blog.jayfields.com/2011/08/clojure-apply-function-to-each-value-of.html*
- HSV/RGB conversion: https://github.com/rm-hull/inkspot/blob/ccb7a452a0930f451bcc6c960024d6029f616cd2/src/inkspot/converter.cljc#L30
- Transitions: https://gist.github.com/raspasov/f9ca712571efd932169e
