(defproject homebridge-artnet "0.0.0"
  :source-paths ["src"]
  :test-paths []
  :resource-paths []
  :compile-path nil
  :target-path nil
  :plugins [[lein-tools-deps "0.4.5"]]
  :dependencies [[prismatic/schema "1.2.0"]
                 [binaryage/oops "0.7.2"]
                 [org.clojure/core.async "1.5.648"]]
  :middleware [lein-tools-deps.plugin/resolve-dependencies-with-deps-edn]
  :lein-tools-deps/config {:config-files [:install :user :project]
                           :aliases      [:dev :test]})
